
import numpy as np
from glob import glob 
import librosa
from scipy import signal 
from scipy.io import wavfile 
import glob
import numpy as np
from matplotlib import pyplot as plt
import scipy.io.wavfile as wav

from keras.optimizers import Adam
from sklearn.model_selection import train_test_split
import keras
from keras.models import Model
from keras.models import Sequential,Model
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import Conv2D
from keras.layers.convolutional import Conv2DTranspose
from keras.layers.convolutional import MaxPooling2D
from keras.layers.core import Activation
from keras.layers.core import Flatten
from keras.layers.core import Dropout
from keras.layers.core import Dense
from keras.layers import Bidirectional
from keras.layers import LSTM
model = Sequential()
from keras.preprocessing.sequence import pad_sequences
from keras.layers.core import Reshape
from keras.layers import Input, TimeDistributed

clean = []
for file in glob.glob("./clean_trainset_wav/*.wav"):
    v, vr = librosa.load(file)
    clean.append(v)
print("import clean")


noise = []
for file in glob.glob("./noisy_trainset_wav/*wav"):
    x, xr = librosa.load(file)
    noise.append(x)
print("import noise")


spec_clean=[]
for i in range(len(clean)):
    frequencies, times, spectrogram = signal.spectrogram(clean[i], vr)
    z= pad_sequences(spectrogram, padding='post', maxlen=1000)
    spec_clean.append(z)


spec_noise=[]
for i in range(len(noise)):
    frequencies, times, spectrogram = signal.spectrogram(noise[i], vr)
    z= pad_sequences(spectrogram, padding='post', maxlen=1000)
    spec_noise.append(z)


spec_noise=np.asarray(spec_noise)
spec_clean=np.asarray(spec_clean)


spec_noise = spec_noise.reshape(spec_noise.shape[0], 1000, 129, 1)
spec_clean = spec_clean.reshape(spec_clean.shape[0], 1000, 129)


(trainX, testX, trainY, testY) = train_test_split(np.array(spec_noise),np.array(spec_clean), test_size=0.1)


model = Sequential()

model.add(Conv2D(256, kernel_size=(11, 32),strides=(1, 16),padding='same',input_shape=(1000,129,1)))
model.add(BatchNormalization())
model.add(Activation('relu'))

model.add(Reshape((1000, 2304)))

model.add(Bidirectional(LSTM(1024, input_shape=(1000,2304),return_sequences=True)))

model.add(Bidirectional(LSTM(1024,return_sequences=True)))

model.add(TimeDistributed(Dense(129,activation = 'relu')))

model.compile(loss=keras.losses.mean_squared_error,
                   optimizer=keras.optimizers.Adam(),
                   metrics=['accuracy'])

print(model.summary())


model.fit(np.asarray(trainX), np.asarray(trainY),
              batch_size=8,
              epochs=5,
              validation_split = 0.01
             )


model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("model.h5")
print("Saved model to disk")

